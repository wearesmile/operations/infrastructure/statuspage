# SMILE Status Page

This repo is deployed via GitLab Pages, and connects with Uptime Robot and a small API that Nathan Monk wrote to grab GitLab issues privately. It displays this information so that our clients can see the health of our infrastructure.

Incidents should be opened on the issue tracker. The initial description will be shared publicly. Discussion threads will be private, so it is an ideal place to discuss incidents.